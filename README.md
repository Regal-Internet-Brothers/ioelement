ioelement
=========

A file-format (And general I/O) framework for the [Monkey programming language](https://github.com/blitz-research/monkey).
